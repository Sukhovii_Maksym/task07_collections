package model;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class MyTreeMap<K extends Comparable<K>,V> implements Map<K,V> {
    private Node<K,V> root;
    private int size;


    public Node<K,V> getNode(Object key){
        Node<K,V> node = root;
        Comparable<? super K> k = (Comparable<? super K>) key;
        while(node !=null){
            int cmprt = k.compareTo(node.getKey());
            if(cmprt<0)
                node=node.getLeftNode();
            else if (cmprt>0)
                node=node.getRightNode();
            else
                return node;
        }

        return null;
    }
    @Override
    public V get(Object key) {
        Node<K,V> node =getNode(key);
         return (node==null?null:node.getValue());
    }

    @Override
    public V put(K key, V value) {
        Node <K,V> node = new Node(key,value);
        if (root==null){
            root = node;
            size=1;
            return null;
        }
        Node<K,V> parent=root;
        do{
            int cmpr = key.compareTo(parent.getKey());
            if(cmpr >0)
                parent=parent.getRightNode();
            else if (cmpr<0)
                parent=parent.getLeftNode();
            else {
                parent.setValue(value);
                return value;
            }
        }while(parent!=null);
        int cmpr = key.compareTo(parent.getKey());
        if (cmpr<0)
            parent.setLeftNode(node);
        else
            parent.setRightNode(node);
        size++;
        return null;
    }
//in develop
    @Override
    public V remove(Object key) {
        size--;
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public int size() {
        return size;
    }

}
