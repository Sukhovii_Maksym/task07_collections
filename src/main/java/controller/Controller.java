package controller;

import model.MyTreeMap;
import model.Node;
import view.Printable;

import java.util.Map;

public class Controller <K extends Comparable<K>,V> implements Functionality<K,V>, Printable {
    private MyTreeMap<K,V> map = new MyTreeMap();


    @Override
    public V get(Object key) {
        return map.get(key);
    }

    @Override
    public Node getNode(Object key) {
        return map.getNode(key);
    }

    @Override
    public V put(K key, V value) {
           return map.put(key,value);
    }

    @Override
    public V remove(K key) {
        return map.remove(key);
    }

    @Override
    public void putAll(Map<K, V> map) {
        this.map = (MyTreeMap<K, V>) map;
    }

    @Override
    public void print() {
        map.forEach((key, value) -> System.out.println(key + ":" + value));
    }
}
